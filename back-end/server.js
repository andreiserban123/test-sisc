const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");

//Initializing server
const app = express();
app.use(bodyParser.json());
const port = 8081;
app.listen(port, () => {
  console.log("Server online on: " + port);
}); 
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end"
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS pasageri_inscrisi(nume VARCHAR(255),prenume VARCHAR(255),telefon VARCHAR(255),email VARCHAR(255),facultate VARCHAR(255),specializare VARCHAR(255),serie VARCHAR(1), grupa INTEGER, an_inceput INTEGER, taxa_inscriere FLOAT)";
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/inscriere", (req, res) => {
  const pasager = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    email: req.body.email,
    facultate: req.body.facultate,
    serie: req.body.serie,
    an_inceput: req.body.an_inceput,
    grupa: req.body.grupa,
    taxa_inscriere: req.body.taxa_inscriere,
    specializare: req.body.specializare
  }
  let error = [];

  console.log(pasager);
  if (!pasager.nume || !pasager.prenume || !pasager.telefon || !pasager.email || !pasager.facultate  || !pasager.serie || !pasager.an_inceput || !pasager.grupa || !pasager.taxa_inscriere || !pasager.specializare) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
    if (pasager.nume.length < 2 || pasager.nume.length > 30) {
      console.log("Nume invalid!");
      error.push("Nume invalid");
    } else if (!pasager.nume.match("^[A-Za-z]+$")) {
      
      console.log("Numele trebuie sa contina doar litere!");
      error.push("Numele trebuie sa contina doar litere!");
    }
    if (pasager.telefon.length < 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
    } else if (!pasager.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");
    }
    if (!pasager.email.includes("@gmail.com") && !pasager.email.includes("@yahoo.com")) {
      console.log("Email invalid!");
      error.push("Email invalid!");
    }
    //^[0-9]+$
    //^[A-Za-z]+$
    if (!pasager.facultate.match("^[A-Za-z]+$")) 
    {
      console.log("Denumirea facultatii trebuie sa contina doar litere!");
      error.push("Denumirea facultatii trebuie sa contina doar litere!");
    }
      
  
    const serii = ['A', 'B', 'C', 'D', 'E', 'F'];
    let ok = false;
    serii.forEach(serie => {
      if (pasager.serie === serie) {
        ok = true;
      }
    })
    if (!ok) {
      console.log("Seria introdusa nu se regaseste in aceasta facultate!");
      error.push("Seria introdusa nu se regaseste in aceasta facultate!");
    }
    if (parseInt(pasager.grupa) ==="NaN") {
      console.log("Grupa trebuie sa contina doar cifre!");
      error.push("Grupa trebuie sa contine doar cifre!");
    }
    if (parseInt(pasager.an_inceput) === "NaN") {
      console.log("Anul nu poate contine litere!");
      error.push("Anul nu poate contine litere!");
    }
    if (pasager.an_inceput != new Date().getFullYear()) 
    {
      console.log("Anul nu este valid!");
      error.push("Anul nu este valid!");
    }
     

    if (pasager.taxa_inscriere != 200) {
      console.log("Taxa de inscriere incorecta.");
      error.push("Taxa de inscriere incorecta.");
    }
  }
  if (error.length === 0) {
    const sql =
      `INSERT INTO pasageri_inscrisi (nume,prenume,telefon,email,facultate,specializare,serie,grupa,an_inceput,taxa_inscriere) VALUES (?,?,?,?,?,?,?,?,?,?)`;
    connection.query(sql,
      [
        pasager.nume,
        pasager.prenume,
        pasager.telefon,
        pasager.email,
        pasager.facultate,
        pasager.serie,
        pasager.an_inceput,
        pasager.grupa,
        pasager.taxa_inscriere,
        pasager.specializare,
      ],
   
      function (err, result) {
        if (!result) throw err;
        console.log("pasager inscris in baza de date!");
        res.status(200).send({
          message: "pasager inscris in baza de date!"
        });
        console.log(sql);
      });
  } else {
    res.status(500).send(error);
    console.log("Eroare la inserarea in baza de date!");
  }
});